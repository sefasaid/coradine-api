// config from .env file
import * as env from 'dotenv';
env.config();
// Express & other imports
import express, { Request, Response } from "express";
import { ApolloServer, AuthenticationError } from 'apollo-server-express';
import mongoose from "mongoose";
import { typeDefs } from './models/graph-model';
import { resolvers } from './resolvers/index';
import { getUser } from './helpers/jwt-token';
import { userRouter } from './routes/user';
import cors from 'cors';
// Express app
const app = express();
const port = process.env.PORT || 3000;
const mongoString: string = process.env.MONGO_STRING as string;
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(userRouter)

// connection Mongodb
mongoose
    .connect(mongoString, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log("MongoDB connected..."))
    .catch(err => console.log(err));


// Apollo Server
let apolloServer: ApolloServer;

// Start Server with apollo
const startServer = async () => {
    apolloServer = new ApolloServer({
        typeDefs, resolvers,
        context: ({ req }) => {
            // get the user token from the headers
            const token = req.headers.authorization;
            if (!token) throw new AuthenticationError('you must be logged in');
            const user = getUser(token);
            if (!user) throw new AuthenticationError('you must be logged in');
            return { user };
        },
    });
    await apolloServer.start()
    apolloServer.applyMiddleware({ app });

    // Express route and listen
    app.get("/", (req: Request, res: Response) => {
        res.send("Hello World");
    });

    app.listen(port, () => {
        console.log(`Server is running at http://localhost:${port}${apolloServer.graphqlPath}`)
    });
}
startServer();
