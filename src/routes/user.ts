import express, { Request, Response } from 'express'
import { generateAccessToken } from '../helpers/jwt-token';
import { generateHash, validateHash } from '../helpers/password';
import { User } from '../models/users';
const router = express.Router()

router.post('/register', async (req: Request, res: Response) => {
    const { username, password } = req.body;
    if (!username || !password)
        return res.status(400).send({ message: 'Invalid Inputs' });

    const newPass = await generateHash(password);
    let user = new User({ username, password: newPass })
    user = await user.save();
    const token = generateAccessToken(user);
    res.json({ user, token });
})

router.post('/login', async (req: Request, res: Response) => {
    const { username, password } = req.body;
    if (!username || !password)
        return res.status(400).send({ message: 'Invalid Inputs' });

    const user = await User.findOne({ username });
    if (!user)
        return res.status(400).send({ message: 'Login failed' });
    const hash = await validateHash(password, user.password);
    if (!hash)
        return res.status(400).send({ message: 'Login failed' });
    const token = generateAccessToken(user);
    res.json({ user, token });
})

export { router as userRouter }