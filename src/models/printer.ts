import mongoose from 'mongoose'
const ObjectId = mongoose.Types.ObjectId;
export interface IPrinter {
    title: string;
    description: string;
    owner: mongoose.Types._ObjectId;
}

interface printerInterface extends mongoose.Model<PrinterDoc> {
}

interface PrinterDoc extends mongoose.Document {
    title: string;
    description: string;
    owner: mongoose.Types._ObjectId;
}

const printerSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    owner: {
        type: ObjectId,
        ref: 'users',
        autopopulate: true
    }
}).set('toJSON', {
    virtuals: true
});
printerSchema.plugin(require('mongoose-autopopulate'));
const Printer = mongoose.model<PrinterDoc, printerInterface>('printers', printerSchema)

export { Printer }

