import { gql } from 'apollo-server-express';
export const typeDefs = gql`
    type User{
       id:ID
       username: String
    }

    type Printer{
        id:ID
        title: String
        description: String
        owner: User!
    }

    input Input{
        title: String
        description: String
    }

    type Query{
        printers:[Printer]
        printer(id:ID):Printer
    }

    type Mutation{
        create(printer:Input):Printer
        edit(id:ID,printer:Input):Printer
        delete(id:ID):ID
    }

`;