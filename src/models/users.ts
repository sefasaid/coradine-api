import mongoose from 'mongoose';

export interface IUser {
    username: string;
    password: string;
}

interface UserInterface extends mongoose.Model<UserDoc> {
}

export interface UserDoc extends mongoose.Document {
    username: string;
    password: string;
}

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
}).set('toJSON', {
    virtuals: true
});

const User = mongoose.model<UserDoc, UserInterface>('users', userSchema)

export { User }

