import bcrypt from 'bcrypt';
const SALT_WORK_FACTOR = 10;

const generateHash = async (password: string) => {
    return await bcrypt.hash(password, SALT_WORK_FACTOR)
}
const validateHash = async (password: string, hash: string) => {
    return await bcrypt.compare(password, hash)
}
export { validateHash, generateHash }