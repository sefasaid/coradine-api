import jwt from 'jsonwebtoken';
import { UserDoc } from '../models/users';
export function getUser(tokenString: string) {
    const token = tokenString.split(' ')[1]
    try {
        return jwt.verify(token, process.env.JWT_SECRET as string)
    } catch (err) {
        return false;
    }
}
export function generateAccessToken(user: UserDoc) {
    return jwt.sign(user.toJSON(), process.env.JWT_SECRET as string);
}