import { ApolloError } from 'apollo-server-express';
import mongoose from 'mongoose'
const ObjectId = mongoose.Types.ObjectId;
import { Printer } from "../models/printer";

export const resolvers = {
    Query: {
        printers: async (parent: any, args: any, context: any) => {
            return await Printer.find({ owner: context.user.id })
        },
        printer: async (parent: any, { id }: any, context: any) => {
            return await Printer.findOne({ _id: id, owner: context.user.id })
        }
    },
    Mutation: {
        create: async (parent: any, { printer }: any, context: any) => {
            const hasSamePrinter = await Printer.countDocuments({ title: printer.title.trim().toLowerCase(), owner: context.user.id });
            if (hasSamePrinter > 0) {
                throw new ApolloError('You have same printer', 'SAME_PRINTER');
            }
            let newPrinter = new Printer({
                ...printer,
                owner: ObjectId(context.user.id)
            });
            const savedPrinter = await newPrinter.save();
            return savedPrinter;
        },
        edit: async (parent: any, { printer, id }: any, context: any) => {
            if (!id || !printer) throw new ApolloError('Some fields are empty', 'EMPTY_FIELDS');
            const printerWithSameName = await Printer.findOne({ title: printer.title.trim().toLowerCase(), owner: context.user.id });
            if (printerWithSameName && printerWithSameName._id.toString() !== id) {
                throw new ApolloError('You have same printer', 'SAME_PRINTER');
            }
            await Printer.findOneAndUpdate({ _id: id, owner: ObjectId(context.user.id) }, { ...printer }, { new: false, useFindAndModify: false });
            return await Printer.findOne({ _id: id })
        },
        delete: async (parent: any, { id }: any, context: any) => {
            if (!id) throw new ApolloError('Some fields are empty', 'EMPTY_FIELDS');
            await Printer.deleteOne({ _id: id });
            return id;
        },
    },
}